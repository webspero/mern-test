import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment';
import { connect } from 'react-redux'
import {disableSubCategory,disableCategory,updateCategoryUIConstraints} from '../../../redux/category/Action'
import {USER_KEY,USER_DATA,CATEGORY_LIST_DATA,SUB_CATEGORY_LIST_DATA,CATEGORY_KEY,REFILL_LOGIN_DATA} from '../../../redux/Types'
import { deleteCategory,getSubCategories, getCategories,getSubCategoriebyid, getCategoriebyid,EditCategory } from '../../../apis/APIs';
import Swal from 'sweetalert2'

class Table extends PureComponent {
    static propTypes = {
        table_id: PropTypes.string,
        type: PropTypes.string
    }

    _category = () => <tr>
        <th>Order ID</th>
        <th>Parent category</th>
        <th>Name</th>
        <th>Created Date</th>
        <th>Actions</th>
    </tr>

    _category_data=()=>{
      const { table_id, type,listData } = this.props;
      const data = listData && listData.data ? listData.data : []
   return <tbody>
    {data.map((ele,index)=>{
        return(
            <tr key={index}>
        <td>{ele._id}</td>
        <td>{ele.parentCategory && ele.parentCategory.length ? ele.parentCategory[0].category : " ------- "}</td>
        <td>{ele.category}</td>
        <td>{ moment(ele.createdAt).format('DD/MM/YYYY')}</td>
        <td>
        <button class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick={()=>{this.deleteCategory(ele._id)}} >
        <i class="fa fa-trash" style={{color:"red"}}></i>
        </button>
        <button class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick={()=>{this.editCategory(ele._id)}}  >
        <i class="fa fa-edit" ></i>
        </button>
        </td>
        </tr>
        )
    })}
</tbody>
}


_sub_category_data=()=>{
  const { table_id, type,listData } = this.props;
  const data = listData && listData.data ? listData.data : []
return <tbody>
{data.map((ele,index)=>{
    return(
        <tr key={index}>
    <td>{ele._id}</td>
    <td>{ele.category}</td>
    <td>{ele.subcategory}</td>
    <td>{ moment(ele.createdAt).format('DD/MM/YYYY')}</td>
    <td>
    <button class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick={()=>{this.deleteSubCategory(ele._id)}} >
    <i class="fa fa-trash" style={{color:"red"}}></i>
    </button>
    <button class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick={()=>{this.editSubCategory(ele._id)}}  >
    <i class="fa fa-edit" ></i>
    </button>
    </td>
    </tr>
    )
})}
</tbody>
}

    _sub_category = () => <tr>
        <th>Order ID</th>
        <th>Category ID</th>
        <th>Name</th>
        <th>Created Date</th>
        <th>Actions</th>
    </tr>

    _handle_table_row = () => {
        const { type } = this.props;

        switch (type) {
            case 'category':
                return this._category();
            case 'sub_category':
                return this._sub_category();
        }
    }

    _handle_table_data = () => {
      const { type } = this.props;

      switch (type) {
          case 'category':
              return this._category_data();
          case 'sub_category':
              return this._sub_category_data();
      }
  }

    category_list = function () {
        const {updateCategoryUIConstraints} = this.props
        getCategories().then((data)=>{
            updateCategoryUIConstraints({
                [CATEGORY_LIST_DATA]:data
            })
            console.log("category list",data)
        }).catch(err=>console.log("category err",err))
       
    }

    sub_category_list = function () {
      const {updateCategoryUIConstraints} = this.props
      getSubCategories().then((data)=>{
          updateCategoryUIConstraints({
              [SUB_CATEGORY_LIST_DATA]:data
          })
          console.log("category list",data)
      }).catch(err=>console.log("category err",err))
  }
   

    editCategory(id){
      const {category_list_data} = this.props
      const userData = JSON.parse(localStorage.getItem(REFILL_LOGIN_DATA)) ;
      const categorylist = category_list_data && category_list_data.data ? category_list_data.data : []
      console.log("category",categorylist)
        getCategoriebyid({categoryId:id}).then((data)=>{
            Swal.fire({
                title: 'Edit category',
                html:`
                <select id="category" class="swal2-input">
                ${categorylist.map((ele,index)=>
                  ele._id === data.data.parent_id
                  ?
                  `<option selected value=${ele._id}>${ele.category}</option>`
                  :
                  `<option  value=${ele._id}>${ele.category}</option>`
                )}
                </select>
                `,
                input: 'text',
                inputValue:data.data.category,
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'update',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                  var parentId = document.getElementById('category').value
                    const body = {
                        category:login,
                        categoryId:id,
                        parent_id:parentId,
                        user_token:userData.usertoken
                    }
                  return EditCategory(body)
                    .then(response => {
                      console.log("respon = >",response)
                      if (response.status != 200) {
                        throw new Error(response.data.message)
                      }
                      return response.data.message
                    })
                    .catch(error => {
                      console.log("error = >",error)
                      Swal.showValidationMessage(
                        `Error: ${error}`
                      )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
              }).then((result) => {
                if (result.value) {
                  console.log("result == >",result)
                  Swal.fire({
                    title: result.value,
                  })
                  this.category_list()
                }else if (result.dismiss === Swal.DismissReason.cancel) {
                  Swal.fire(
                    'Cancelled',
                    'Your category is safe :)',
                    'error'
                  )
                }
              })
        }).catch(err=>console.log("error",err))
       
    }

    deleteCategory(id){
        const {disableCategory } = this.props
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this category',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
          }).then((result) => {
            if (result.value) {
                disableCategory(id)
              Swal.fire(
                'Deleted!',
                'Your category has been deleted.',
                'success'
              )
              this.category_list()
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              Swal.fire(
                'Cancelled',
                'Your category is safe :)',
                'error'
              )
            }
          })
    }

    deleteSubCategory(id){
      const {disableSubCategory } = this.props
      Swal.fire({
          title: 'Are you sure?',
          text: 'You want to delete this category',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
        }).then((result) => {
          if (result.value) {
            disableSubCategory(id)
            Swal.fire(
              'Deleted!',
              'Your Subcategory has been deleted.',
              'success'
            )
            this.sub_category_list()
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
              'Cancelled',
              'Your Subcategory is safe :)',
              'error'
            )
          }
        })
  }

  listingcategory(data){
     return data.map((ele,index)=>{
       return (
       <option value={ele._id}>{ele.category}</option>
       )
     })
  }
  editSubCategory(id){
    const {category_list_data} = this.props
    const categorylist = category_list_data && category_list_data.data ? category_list_data.data : []
    console.log("category",categorylist)
    getSubCategoriebyid({subcategoryId:id}).then((data)=>{
      console.log("category",data)
        Swal.fire({
            title: 'Edit Subcategory',
            html:`
            <select id="category" class="swal2-input">
            ${categorylist.map((ele,index)=>
              ele._id === data.data.category
              ?
              `<option selected value=${ele._id}>${ele.category}</option>`
              :
              `<option  value=${ele._id}>${ele.category}</option>`
            )}
            </select>
            `,
            input: 'text',
            inputValue:data.data.subcategory,
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'update',
            cancelButtonText: 'No, keep it',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
              var t = document.getElementById('category').value
              console.log("login ===> ",login,t)

                const body = {
                    category:login,
                    categoryId:id
                }
              return EditCategory()
                .then(response => {
                  if (!response.status == 200) {
                    throw new Error(response.error)
                  }
                  return response.data.message
                })
                .catch(error => {
                  Swal.showValidationMessage(
                    `Request failed: ${error}`
                  )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
          }).then((result) => {
            console.log("result", result)
            if (result) {
              Swal.fire({
                title: `Category update successfully`,
              })
              this.category_list()
            }else if(result.dismiss === Swal.DismissReason.cancel){
              Swal.fire(
                'Cancelled',
                'Your imaginary file is safe :)',
                'error'
              )
            }
          })
    
    
        }).catch(err=>console.log("error",err))
   
}

    render() {
        const { table_id, type,listData } = this.props;
        const data = listData && listData.data ? listData.data : []
        console.log("table",data)
        return (
            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                    <div className="kt-portlet__body">
                        {/* <!--begin: Datatable --> */}
                        <table className="table table-striped- table-bordered table-hover table-checkable" id={table_id}>
                            <thead>
                                {this._handle_table_row()}
                            </thead>
                           {this._handle_table_data()}
                        </table>

                        {/* <!--end: Datatable --> */}
                    </div>
                </div>
            </div>
        )
    }
}

const mapToProps = ({ user, category }) => {
    const user_data = user && user[USER_KEY] ? user[USER_KEY] : undefined;
    const user_token = user_data && user_data[USER_DATA] && user_data[USER_DATA].user_token ? user_data[USER_DATA].user_token : false;

    const category_data = category && category[CATEGORY_KEY] ? category[CATEGORY_KEY] : undefined;

    const category_list_data = category_data && category_data[CATEGORY_LIST_DATA] ? category_data[CATEGORY_LIST_DATA] : []
    return ({
        user_token,
        category_list_data
    });
}

export default connect(mapToProps, {
    disableCategory,
    disableSubCategory,
    updateCategoryUIConstraints
})(Table);


// <div className="kt-portlet__head kt-portlet__head--lg">
//     <div className="kt-portlet__head-label">
//         <span className="kt-portlet__head-icon">
//             <i className="kt-font-brand flaticon2-line-chart"></i>
//         </span>
//         <h3 className="kt-portlet__head-title">
//             Category List
// 												</h3>
//     </div>
//     <div className="kt-portlet__head-toolbar">
//         <div className="kt-portlet__head-wrapper">
//             <div className="kt-portlet__head-actions">
//                 <div className="dropdown dropdown-inline">
//                     <button type="button" className="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                         <i className="la la-download"></i> Export
// 															</button>
//                     <div className="dropdown-menu dropdown-menu-right">
//                         <ul className="kt-nav">
//                             <li className="kt-nav__section kt-nav__section--first">
//                                 <span className="kt-nav__section-text">Choose an option</span>
//                             </li>
//                             <li className="kt-nav__item">
//                                 <a href="#" className="kt-nav__link">
//                                     <i className="kt-nav__link-icon la la-file-text-o"></i>
//                                     <span className="kt-nav__link-text">CSV</span>
//                                 </a>
//                             </li>
//                         </ul>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     </div>
// </div>