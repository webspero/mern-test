import { SIGNUP_KEY, SIGNUP_REQEUST_LOADING, SIGNUP_REQUEST_STATUS, STATUS, MESSAGE, EMPTY, SIGNUP_FORM, SIGNUP_FORM_PASSWORD, SIGNUP_ERRORS, SIGNUP_FORM_EMAIL, SIGNUP_UPDATE, LOG_OUT, SIGNUP_REQEUST_SESSION_LOADING, SIGNUP_REQUEST_SESSION_STATUS, SIGNUP_RESET, SIGNUP_ROOT, SIGNUP_FORM_MOBILE } from "../Types";

const INITIAL_STATE = {
    [SIGNUP_KEY]: {
        [SIGNUP_FORM]: {
            [SIGNUP_FORM_EMAIL]: "",
            [SIGNUP_FORM_PASSWORD]: "",
            [SIGNUP_FORM_MOBILE]:""
        },
        [SIGNUP_REQEUST_LOADING]: false,
        [SIGNUP_REQUEST_STATUS]: {
            [STATUS]: EMPTY,
            [MESSAGE]: ""
        },
        [SIGNUP_REQEUST_SESSION_LOADING]: false,
        [SIGNUP_REQUEST_SESSION_STATUS]: {
            [STATUS]: EMPTY,
            [MESSAGE]: ""
        },
        [SIGNUP_ERRORS]: []
    }
};

const Reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SIGNUP_UPDATE:
            return { ...state, [SIGNUP_KEY]: action.payload }
        case SIGNUP_RESET:
        case LOG_OUT:
            return {
                ...state,
                [SIGNUP_KEY]: {
                    [SIGNUP_FORM]: {
                        [SIGNUP_FORM_EMAIL]: "",
                        [SIGNUP_FORM_PASSWORD]: "",
                        [SIGNUP_FORM_MOBILE]:""
                    },
                    [SIGNUP_REQEUST_LOADING]: false,
                    [SIGNUP_REQUEST_STATUS]: {
                        [STATUS]: EMPTY,
                        [MESSAGE]: ""
                    },
                    [SIGNUP_REQEUST_SESSION_LOADING]: false,
                    [SIGNUP_REQUEST_SESSION_STATUS]: {
                        [STATUS]: EMPTY,
                        [MESSAGE]: ""
                    },
                    [SIGNUP_ERRORS]: []
                }
            }
        default:
            return state;
    }
}

export default Reducer;