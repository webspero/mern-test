import { refillLogin,signupapi, refillSessionLogin, refillLogout } from '../../apis/APIs'
import { SIGNUP_KEY, SIGNUP_FORM, SIGNUP_ROOT, SIGNUP_UPDATE, SIGNUP_REQUEST_STATUS, SIGNUP_REQEUST_LOADING, STATUS, MESSAGE, EMPTY, SIGNUP_FORM_EMAIL, SIGNUP_FORM_PASSWORD, USER_DATA, ERROR, SUCCESS, SYSTEM_DATA_IS_AUTHENTICATED, LOGIN_REQUEST_SESSION_STATUS, LOGIN_REQEUST_SESSION_LOADING, LOGIN_REQEUST_LOGOUT_LOADING, LOG_OUT, SIGNUP_RESET, SIGNUP_FORM_MOBILE } from "../Types";
import Utils from '../../components/util/Utils';
import { RefillStorage } from '../../apis';
import { updateUserData } from '../user/Action';
import { updateSystemData } from '../system/Action';

/** Login */
export const signup = (obj) => {
    return (dispatch, getState) => {
        try {
            const loginInfo = getState()[SIGNUP_ROOT][SIGNUP_KEY];
            const formData = loginInfo[SIGNUP_FORM];

            //Intialize the request status and loading
            dispatch(updateUIConstraints({
                [SIGNUP_REQUEST_STATUS]: {
                    [STATUS]: EMPTY,
                    [MESSAGE]: ""
                },
                [SIGNUP_REQEUST_LOADING]: true
            }));

            const body = {
                "username": formData[SIGNUP_FORM_EMAIL],
                "password": formData[SIGNUP_FORM_PASSWORD],
                "mobile":formData[SIGNUP_FORM_MOBILE],
                "userrole":"user"
            }

            signupapi(body).then(async(res) => {
                Utils.log("Refill Login Response ===> ", res);

                if (res && res.status === 200) {
                    try {
                        // await RefillStorage.storeRefillLoginData(res.data);
                        // const userData = await RefillStorage.getRefillLoginData();
                        // dispatch(updateUserData({
                        //     [USER_DATA]: userData && userData.response ? userData.response : userData
                        // }));
                    } catch (error) {
                        Utils.log("Refill Login Storage ===> Error ", error);
                        dispatch(updateUIConstraints({
                            [SIGNUP_REQUEST_STATUS]: {
                                [STATUS]: ERROR,
                                [MESSAGE]: "Unable to store data, please try again"
                            },
                            [SIGNUP_REQEUST_LOADING]: false
                        }));
                        return;
                    }
                    dispatch(updateUIConstraints({
                        [SIGNUP_REQUEST_STATUS]: {
                            [STATUS]: SUCCESS,
                            [MESSAGE]: res
                        },
                        [SIGNUP_REQEUST_LOADING]: false
                    }));

                    //Update System Data
                    dispatch(updateSystemData({
                        [SYSTEM_DATA_IS_AUTHENTICATED]: false
                    }));
                } else {
                    dispatch(updateUIConstraints({
                        [SIGNUP_REQUEST_STATUS]: {
                            [STATUS]: ERROR,
                            [MESSAGE]: res
                        },
                        [SIGNUP_REQEUST_LOADING]: false
                    }));
                }
            }).catch(error => {
                Utils.log("Refill Login ===> error", error);
                dispatch(updateUIConstraints({
                    [SIGNUP_REQUEST_STATUS]: {
                        [STATUS]: ERROR,
                        [MESSAGE]: ""
                    },
                    [SIGNUP_REQEUST_LOADING]: false
                }));
            });
        } catch (error) {
            Utils.log("Update Form Data ===> error ", error);
            dispatch(updateUIConstraints({
                [SIGNUP_REQUEST_STATUS]: {
                    [STATUS]: ERROR,
                    [MESSAGE]: ""
                },
                [SIGNUP_REQEUST_LOADING]: false
            }));
        }
    }
}



/** Manage Form Data */
export const updateFormData = (obj) => {
    return (dispatch, getState) => {
        try {
            const formData = getState()[SIGNUP_ROOT][SIGNUP_KEY];
            const data = Object.assign(formData[SIGNUP_FORM], obj);

            dispatch(updateLoginState(Object.assign(formData, {
                [SIGNUP_FORM]: data
            })));
        } catch (error) {
            Utils.log("Update Form Data ===> error ", error);
        }
    }
}

/** Manage UI Constraints */
export const updateUIConstraints = (obj) => {
    return (dispatch, getState) => {
        try {
            const formData = getState()[SIGNUP_ROOT][SIGNUP_KEY];
            const data = Object.assign(formData, obj);

            dispatch(updateLoginState(data));
        } catch (error) {
            Utils.log("Update UI Constraints ===> error ", error);
        }
    }
}

/** Update login data state */
const updateLoginState = (obj) => {
    return (dispatch, getState) => {
        try {
            const formData = getState()[SIGNUP_ROOT][SIGNUP_KEY];

            dispatch({
                type: SIGNUP_UPDATE,
                payload: Object.assign(formData, obj)
            })
        } catch (error) {
            Utils.log("Update Login State ===> error ", error);
        }
    }
}

/** Reset login data state */
export const resetLoginState = (obj) => {
    return (dispatch, getState) => {
        try {
            dispatch({
                type: SIGNUP_RESET,
                payload: {}
            })
        } catch (error) {
            Utils.log("Update Login State ===> error ", error);
        }
    }
}

/** Update logout data state */
export const updateLogoutState = () => {
    return (dispatch, getState) => {
        try {
            dispatch({
                type: LOG_OUT,
                payload: {}
            })
        } catch (error) {
            Utils.log("Update Login State ===> error ", error);
        }
    }
}