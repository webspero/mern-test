import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { SIGNUP_KEY, SIGNUP_FORM, SIGNUP_FORM_EMAIL, SIGNUP_FORM_PASSWORD, SIGNUP_ERRORS, SUCCESS, ERROR, STATUS, SIGNUP_REQEUST_LOADING, SIGNUP_REQUEST_STATUS, MESSAGE, SYSTEM_DATA_PAGE_TITLE, SIGNUP_FORM_MOBILE } from '../../redux/Types';
import { Helper } from '../../apis'
import { updateFormData, updateUIConstraints, signup, resetLoginState } from '../../redux/signup/Action';
import Utils from '../../components/util/Utils';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import { updateSystemData } from '../../redux/system/Action';

class Signup extends PureComponent {
    static propTypes = {

    }

    componentDidMount = () => {
        this.init();
    }

    init = () => {
        const { updateSystemData } = this.props;

        updateSystemData({
            [SYSTEM_DATA_PAGE_TITLE]: "Refill | Login"
        });
    }

    _openPage = (url) => {
        const { history } = this.props;

        if (!url) return;

        history.push(url);
    }

    submit = (e) => {
        e.preventDefault();

        const { username,mobile_number, password, updateUIConstraints, signup, loading } = this.props;
        if (loading) return;

        const requestBody = { username, user_password: password,mobile_number };

        Helper.validate(Object.keys(requestBody), requestBody)
            .then(({ status, response }) => {
                if (status) {
                    updateUIConstraints({
                        [SIGNUP_ERRORS]: []
                    });

                    signup();
                    setTimeout(() => {
                        this._openPage('/login')
                    }, 1000);
                    
                } else updateUIConstraints({
                    [SIGNUP_ERRORS]: response && response.length ? response : []
                });
            }).catch(err => console.log(err));
    }

    /** On error */
    isError = (key) => {
        const { errors } = this.props;

        if (errors && errors.length) {
            return errors.findIndex(ele => (ele.fieldName === key)) > -1 ? { status: true, message: errors[errors.findIndex(ele => (ele.fieldName === key))].message } : { status: false, message: "" };
        } else return { status: false, message: "" }
    }

    onChangeText = (key, value) => {
        const { updateFormData } = this.props;

        updateFormData({
            [key]: value
        });
    }

    componentDidUpdate = (prevProps) => {
        const { reqeustStatus, loading, navigation } = this.props;

        const prevReqeustStatus = prevProps && prevProps.reqeustStatus ? prevProps.reqeustStatus : {};
        if (reqeustStatus[STATUS] !== prevReqeustStatus[STATUS]) {
            switch (reqeustStatus[STATUS]) {
                case SUCCESS:

                    break;
                case ERROR:
                    const status = reqeustStatus[MESSAGE] && reqeustStatus[MESSAGE].status ? reqeustStatus[MESSAGE].status : 500;
                    switch (status) {
                        case 401:
                            ToastsStore.error("Either username or password is incorrect");
                            break;
                        default:
                            ToastsStore.error("Internal server error");
                    }
                    break;
            }
        }
    }

    _handleErrorMessage = (key) => {
        const data = this.isError(key);
console.log(data)
        if (data && data.status) return <p className="text-right text-error">{data.message}</p>;

        return <div />
    }

    componentWillUnmount = () => {
        this.resetLoginData();
    }

    resetLoginData = () => {
        const { resetLoginState } = this.props;

        resetLoginState();
    }

    render() {
        const { username,mobile_number, password, loading } = this.props;

        return (
            <div className="kt-page-content-white kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
                <ToastsContainer store={ToastsStore} lightBackground position={ToastsContainerPosition.TOP_RIGHT} />

                {/* begin:: Page */}
                <div className="kt-grid kt-grid--ver kt-grid--root kt-page height-100">
                    <div className="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
                        <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style={{ backgroundImage: 'url(assets/media//bg/bg-3.jpg)' }}>
                            <div className="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                <div className="kt-login__container">
                                    <div className="kt-login__logo">
                                        <a href="#">
                                            <img src="/small-logo.png" />
                                        </a>
                                    </div>
                                    <div className="kt-login__signin">
                                        <div className="kt-login__head">
                                            <h3 className="kt-login__title">Sign In Signup</h3>
                                        </div>
                                        <form className="kt-form" onSubmit={this.submit.bind(this)}>
                                            <div className="input-group">
                                                <input className="form-control" type="text" placeholder="username" name="username" onChange={(e) => this.onChangeText(SIGNUP_FORM_EMAIL, e.target.value)} autoComplete="off" value={username} />
                                            </div>
                                            {/* display error */}
                                            {this._handleErrorMessage("username")}
                                            <div className="input-group">
                                                <input className="form-control" type="password" placeholder="Password" onChange={(e) => this.onChangeText(SIGNUP_FORM_PASSWORD, e.target.value)} name="password" value={password} />
                                            </div>
                                            {/* display error */}
                                            {this._handleErrorMessage("user_password")}
                                            <div className="input-group">
                                                <input className="form-control" type="text" placeholder="mobile" name="mobile_number" onChange={(e) => this.onChangeText(SIGNUP_FORM_MOBILE, e.target.value)} autoComplete="off" value={mobile_number} />
                                            </div>
                                            {/* display error */}
                                            {this._handleErrorMessage("mobile_number")}
                                           
                                            <div className="kt-login__actions">
                                                <button type="submit" id="kt_login_signin_submit" className="btn btn-brand btn-elevate kt-login__btn-primary">{loading ? "Loading..." : "Sign Up"}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* end:: Page */}
            </div>
        )
    }
}

const mapToProps = (state) => {
    const { login } = state;
    const login_data = login && login[SIGNUP_KEY] ? login[SIGNUP_KEY] : undefined;
    const formData = login_data && login_data[SIGNUP_FORM] ? login_data[SIGNUP_FORM] : undefined;
    const username = formData && formData[SIGNUP_FORM_EMAIL] ? formData[SIGNUP_FORM_EMAIL] : undefined;
    const password = formData && formData[SIGNUP_FORM_PASSWORD] ? formData[SIGNUP_FORM_PASSWORD] : undefined;
    const mobile_number = formData && formData[SIGNUP_FORM_MOBILE] ? formData[SIGNUP_FORM_MOBILE] : undefined;
    const errors = login_data && login_data[SIGNUP_ERRORS] ? login_data[SIGNUP_ERRORS] : [];
    const loading = login_data && login_data[SIGNUP_REQEUST_LOADING] ? login_data[SIGNUP_REQEUST_LOADING] : false;
    const reqeustStatus = login_data && login_data[SIGNUP_REQUEST_STATUS] ? login_data[SIGNUP_REQUEST_STATUS] : {};

    return ({
        username,
        password,
        errors,
        loading,
        reqeustStatus,
        mobile_number
    });
}

export default connect(mapToProps, {
    updateFormData,
    updateUIConstraints,
    signup,
    updateSystemData,
    resetLoginState
})(Signup);