import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Footer from '../../components/dashboard/Footer'
import Header from '../../components/dashboard/Header'
import Sidebar from '../../components/dashboard/Sidebar'
import asyncComponent from '../../components/base_components/AsyncComponent';
import { Switch } from "react-router-dom";
import { AuthRoute } from '../../components/base_components'
import { connect } from 'react-redux'
import { USER_KEY, USER_DATA, ACTIVE } from '../../redux/Types'

const routes = {
    index: '/',
    user:'/user',
    reset_password: '/reset_password',
    change_password: '/profile/change_password',
    add_category: '/category/add',
    list_category: '/category/list',
    edit_category: '/category/edit',
}

class Routes extends PureComponent {

    _replacePage = (url) => {
        const { history } = this.props;

        if (!url) return;

        history.replace(url);
    }

    componentDidMount = () => {
        const { is_reset_password } = this.props;
        if (is_reset_password === ACTIVE) this._replacePage('/reset_password');
    }

    render() {
        return (
            <Switch>
                <AuthRoute
                    exact
                    path={routes.index}
                    component={asyncComponent(() => import("./Home"))}
                    props={this.props}
                />
                <AuthRoute
                    exact
                    path={routes.user}
                    component={asyncComponent(() => import("./User"))}
                    props={this.props}
                />
                <AuthRoute
                    exact
                    path={routes.change_password}
                    component={asyncComponent(() => import("./settings/ChangePassword"))}
                    props={this.props}
                />
                <AuthRoute
                    exact
                    path={routes.reset_password}
                    component={asyncComponent(() => import("./settings/ResetPassword"))}
                    props={this.props}
                />
                <AuthRoute
                    exact
                    path={routes.add_category}
                    component={asyncComponent(() => import("./category/Add"))}
                    props={this.props}
                />
                <AuthRoute
                    exact
                    path={routes.list_category}
                    component={asyncComponent(() => import("./category/List"))}
                    props={this.props}
                />
                
                
            </Switch>
        )
    }
}

const mapToProps = ({ user }) => {
    const user_data = user && user[USER_KEY] ? user[USER_KEY] : undefined;
    const is_reset_password = user_data && user_data[USER_DATA] && user_data[USER_DATA].reset_password ? user_data[USER_DATA].reset_password : 0;

    return ({
        is_reset_password
    });
}

export default connect(mapToProps)(Routes);