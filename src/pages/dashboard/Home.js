import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {REFILL_LOGIN_DATA} from '../../redux/Types'

export default class Home extends PureComponent {
    static propTypes = {

    }

    _openPage = (url) => {
        const { history } = this.props;

        if (!url) return;

        history.push(url);
    }


    render() {
        return (
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">
                                        Dashboard </h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>
                                    <div class="kt-subheader__breadcrumbs">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="row row-full-height">
                                        <div class="col-sm-12 col-md-6 col-lg-3">
                                            <div class="kt-portlet kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                <div class="kt-portlet__body kt-portlet__body--fluid">
                                                    <div class="kt-widget26">
                                                        <div class="kt-widget26__content">
                                                            <span class="kt-widget26__number">1570</span>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-3">
                                            <div class="kt-portlet kt-portlet--height-fluid-full kt-portlet--border-bottom-danger">
                                                <div class="kt-portlet__body kt-portlet__body--fluid">
                                                    <div class="kt-widget26">
                                                        <div class="kt-widget26__content">
                                                            <span class="kt-widget26__number">640</span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-3">
                                            <div class="kt-portlet kt-portlet--height-fluid-full kt-portlet--border-bottom-success">
                                                <div class="kt-portlet__body kt-portlet__body--fluid">
                                                    <div class="kt-widget26">
                                                        <div class="kt-widget26__content">
                                                            <span class="kt-widget26__number">234</span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-3">
                                            <div class="kt-portlet kt-portlet--height-fluid-full kt-portlet--border-bottom-warning">
                                                <div class="kt-portlet__body kt-portlet__body--fluid">
                                                    <div class="kt-widget26">
                                                        <div class="kt-widget26__content">
                                                            <span class="kt-widget26__number">350</span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="kt-portlet kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                  
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row row-full-height">
                                                <div class="col-12">
                                                    <div class="rounded kt-portlet--height-fluid-full">
                                                        <h5 class="">Total Earning</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-full-height">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="border mb-2 rounded kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                                            <div class="kt-widget26">
                                                                <div class="kt-widget26__content">
                                                                    <span class="kt-widget26__number"> 1570</span>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="border mb-2 rounded kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                                            <div class="kt-widget26">
                                                                <div class="kt-widget26__content">
                                                                    <span class="kt-widget26__number"> 12570</span>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="kt-portlet kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Category Products
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row row-full-height">
                                                <div class="col-sm-12 col-md-6 col-lg-4">
                                                    <div class="border mb-2 rounded kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                                            <div class="kt-widget26">
                                                                <div class="kt-widget26__content">
                                                                    <span class="kt-widget26__number">1570</span>
                                                                  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-4">
                                                    <div class="border mb-2 rounded kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                                            <div class="kt-widget26">
                                                                <div class="kt-widget26__content">
                                                                    <span class="kt-widget26__number">12570</span>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-4">
                                                    <div class="border mb-2 rounded kt-portlet--height-fluid-full kt-portlet--border-bottom-brand">
                                                        <div class="kt-portlet__body kt-portlet__body--fluid">
                                                            <div class="kt-widget26">
                                                                <div class="kt-widget26__content">
                                                                    <span class="kt-widget26__number">1270</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>           
                                </div>
                            </div>
                        </div>
            </div>
        )
    }
}
